package it.unibo.oop.lab.advanced;

import java.io.FileNotFoundException;
import java.util.Arrays;
import java.util.List;

/**
 */
public final class DrawNumberApp implements DrawNumberViewObserver {

    private static final int MIN = 0;
    private static final int MAX = 100;
    private static final int ATTEMPTS = 10;
    private final DrawNumber model;
    private final List<DrawNumberView> views;

    /**
     * 
     */
    public DrawNumberApp(final DrawNumberView... views) {
        this.model = new DrawNumberImpl(MIN, MAX, ATTEMPTS);
        this.views = Arrays.asList(Arrays.copyOf(views, views.length));
        for (final DrawNumberView view: views) {
            view.setObserver(this);
            view.start();
        }
    }

    @Override
    public void newAttempt(final int n) {
        try {
            final DrawResult result = model.attempt(n);
            for(DrawNumberView view: views) {
                view.result(result);
            }
        } catch (IllegalArgumentException e) {
            for(DrawNumberView view: views) {
                view.numberIncorrect();
            }
        } catch (AttemptsLimitReachedException e) {
            for(DrawNumberView view: views) {
                view.limitsReached();  
            }
        }
    }

    @Override
    public void resetGame() {
        this.model.reset();
    }

    @Override
    public void quit() {
        System.exit(0);
    }

    /**
     * @param args
     *            ignored
     */
    public static void main(final String... args) {
        try {
            new DrawNumberApp(
                new DrawNumberViewImpl(),
                new DrawNumberViewImpl(),
                new PrintStreamView("output.log"),
                new Stdout()
            );
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

}

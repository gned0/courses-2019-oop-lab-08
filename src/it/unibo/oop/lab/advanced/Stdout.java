package it.unibo.oop.lab.advanced;

public class Stdout implements DrawNumberView {

    
    public void setObserver(final DrawNumberViewObserver observer) {
    }

    @Override
    public void start() {
        // TODO Auto-generated method stub

    }

    @Override
    public void numberIncorrect() {
        System.out.println("You must enter a number");
    }

    @Override
    public void result(DrawResult res) {
        System.out.println(res.getDescription());
        
    }

    @Override
    public void limitsReached() {
        // TODO Auto-generated method stub

    }

    @Override
    public void displayError(String message) {
        System.out.println("Error: " + message);
    }

}
